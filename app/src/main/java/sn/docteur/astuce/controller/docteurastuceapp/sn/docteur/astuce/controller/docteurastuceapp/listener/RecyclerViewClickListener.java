package sn.docteur.astuce.controller.docteurastuceapp.sn.docteur.astuce.controller.docteurastuceapp.listener;

import android.view.View;

public interface RecyclerViewClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}