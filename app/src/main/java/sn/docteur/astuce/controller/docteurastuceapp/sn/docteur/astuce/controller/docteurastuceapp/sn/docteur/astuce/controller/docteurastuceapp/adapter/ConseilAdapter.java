package sn.docteur.astuce.controller.docteurastuceapp.sn.docteur.astuce.controller.docteurastuceapp.sn.docteur.astuce.controller.docteurastuceapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import sn.docteur.astuce.controller.docteurastuceapp.R;
import sn.docteur.astuce.controller.docteurastuceapp.sn.docteur.astuce.controller.docteurastuceapp.model.Conseil;

public class ConseilAdapter extends RecyclerView.Adapter<ConseilAdapter.ConseilViewHolder>{

    private List<Conseil> conseilList;
    private Context context;
    public ConseilAdapter(List<Conseil> conseilList, Context context) {
        this.conseilList = conseilList;
        this.context = context;
    }

    @Override
    public ConseilViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_cardview_item, null);
        ConseilViewHolder userViewHolder = new ConseilViewHolder(view);
        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(ConseilViewHolder holder, int position) {
        Conseil conseil = conseilList.get(position);

        holder.tvProfileName.setText(conseil.getTitre());
        holder.tvPhoneNumber.setText(conseil.getContenu());

    }

    @Override
    public int getItemCount() {
        return conseilList.size();
    }

    public static class ConseilViewHolder extends RecyclerView.ViewHolder {

        ImageView ivProfilePic;
        TextView tvProfileName;
        TextView tvPhoneNumber;
        TextView tvEmailId;
        public ConseilViewHolder(View itemView) {
            super(itemView);
            tvProfileName = (TextView) itemView.findViewById(R.id.cardview_list_title);
            tvPhoneNumber = (TextView) itemView.findViewById(R.id.short_description);
        }
    }
}