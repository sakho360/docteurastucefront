package sn.docteur.astuce.controller.docteurastuceapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sn.docteur.astuce.controller.docteurastuceapp.server.ServeurAcces;
import sn.docteur.astuce.controller.docteurastuceapp.sn.docteur.astuce.controller.docteurastuceapp.listener.RecyclerViewClickListener;
import sn.docteur.astuce.controller.docteurastuceapp.sn.docteur.astuce.controller.docteurastuceapp.listener.RecyclerViewTouchListener;
import sn.docteur.astuce.controller.docteurastuceapp.sn.docteur.astuce.controller.docteurastuceapp.model.Conseil;
import sn.docteur.astuce.controller.docteurastuceapp.sn.docteur.astuce.controller.docteurastuceapp.sn.docteur.astuce.controller.docteurastuceapp.adapter.ConseilAdapter;


public class ConseilFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    public static android.support.v4.app.Fragment newInstance() {
        ConseilFragment fragment = new ConseilFragment();
        return fragment;
    }

    View rootView;
    private List<Conseil> list;
    RecyclerView recyclerView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_conseil, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rvAllConseils);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddConseilActivity.class);
                startActivity(intent);
            }
        });
        ReadConseilFromDB();

        return rootView;
    }


    private void ReadConseilFromDB() {
        String url = ServeurAcces.url+"conseils";

        //  progress.show();
        JsonObjectRequest jreq = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                             int success = response.getInt("status");
                            String message = response.getString("message");

                            list = new ArrayList<Conseil>();
                            if (success == 0) {
                                JSONArray ja = response.getJSONArray("conseil_list");

                                for (int i = 0; i < ja.length(); i++) {

                                    JSONObject jobj = ja.getJSONObject(i);
                                    Conseil cons = new Conseil();
                                    cons.setTitre(jobj.getString("titre"));
                                    cons.setContenu(jobj.getString("contenu"));

                                    list.add(cons);

                                } // for loop ends
                               try {
                                    if(list.size()>0) {
                                        ConseilAdapter allUsersAdapter = new ConseilAdapter(list, getActivity());
                                        recyclerView.setAdapter(allUsersAdapter);
                                        allUsersAdapter.notifyDataSetChanged();


                                    }
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    Log.v("Erreur liste: ", "Error");


                                }
/*
                                mSwipeRefreshLayout.setColorSchemeResources(R.color.background_selected, R.color.dark_grey, R.color.background_selected);
                                mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

                                    @Override
                                    public void onRefresh() {
                                        lv.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {

                                                ReadDataFromDB();
                                                //  new ReadLibrary().execute();
                                                mSwipeRefreshLayout.setRefreshing(false);


                                            }
                                        }, 2500);
                                    }

                                });
*/
                                //Click listener

                                recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(), recyclerView, new RecyclerViewClickListener() {
                                    @Override
                                    public void onClick(View view, int position) {
                                        Log.v("Position",position+"");
                                        Log.v("Positio ---- ",list.get(position).getTitre()+"");
                                        Intent intent = new Intent(getActivity(),DetailConseilActivity.class);
                                        intent.putExtra("title", list.get(position).getTitre());
                                        intent.putExtra("contenu", list.get(position).getContenu());
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onLongClick(View view, int position) {
                                        Toast.makeText(getActivity(), list.get(position) + " is long pressed!", Toast.LENGTH_SHORT).show();

                                    }
                                }));



                            } // if ends
                            else {
                                message = response.getString("message");
                                Log.v("message liste", message);
                                //  progress.dismiss();

                                final String finalMessage = message;
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {

                                        Toast.makeText(getActivity(), finalMessage, Toast.LENGTH_SHORT).show();

                                    }
                                });
                                Intent i = new Intent(getActivity(), AccueilActivity.class);
                                startActivity(i);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // progress.dismiss();

                //frameAnimation.stop();
                //  Toast.makeText(getActivity(), "Erreur réseau, rechargement en cours!!", Toast.LENGTH_SHORT).show();
                ReadConseilFromDB();


            }
        });
        jreq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getActivity()).add(jreq);
        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(jreq);

    }

/*
    private List<Conseil> getConseilInformation() {

        List<Conseil> userList = new ArrayList<>();
        userList.add(new Conseil(
                "LES MÉDICAMENTS GÉNÉRIQUES",
                "Le médicament générique doit donc avoir la même activité thérapeutique que la spécialité princeps, équivalence démontrée par une étude de bioéquivalence. Etant donné que le médicament générique offre la même efficacité et la même qualité, ce dernier est accepté en France. L’efficacité, la qualité et la sécurité des génériques français sont scientifiquement contrôlées et démontrées. Ils ont les mêmes équivalences que le princeps ou médicament de référence. Ils équivalent également aux différentes étapes de vie du princeps."));
        userList.add(new Conseil(
                "LES MÉDICAMENTS GÉNÉRIQUES",
                "Le médicament générique doit donc avoir la même activité thérapeutique que la spécialité princeps, équivalence démontrée par une étude de bioéquivalence. Etant donné que le médicament générique offre la même efficacité et la même qualité, ce dernier est accepté en France. L’efficacité, la qualité et la sécurité des génériques français sont scientifiquement contrôlées et démontrées. Ils ont les mêmes équivalences que le princeps ou médicament de référence. Ils équivalent également aux différentes étapes de vie du princeps."));
        userList.add(new Conseil(
                "LES MÉDICAMENTS GÉNÉRIQUES",
                "Le médicament générique doit donc avoir la même activité thérapeutique que la spécialité princeps, équivalence démontrée par une étude de bioéquivalence. Etant donné que le médicament générique offre la même efficacité et la même qualité, ce dernier est accepté en France. L’efficacité, la qualité et la sécurité des génériques français sont scientifiquement contrôlées et démontrées. Ils ont les mêmes équivalences que le princeps ou médicament de référence. Ils équivalent également aux différentes étapes de vie du princeps."));
        userList.add(new Conseil(
                "LES MÉDICAMENTS GÉNÉRIQUES",
                "Le médicament générique doit donc avoir la même activité thérapeutique que la spécialité princeps, équivalence démontrée par une étude de bioéquivalence. Etant donné que le médicament générique offre la même efficacité et la même qualité, ce dernier est accepté en France. L’efficacité, la qualité et la sécurité des génériques français sont scientifiquement contrôlées et démontrées. Ils ont les mêmes équivalences que le princeps ou médicament de référence. Ils équivalent également aux différentes étapes de vie du princeps."));
        userList.add(new Conseil(
                "LES MÉDICAMENTS GÉNÉRIQUES",
                "Le médicament générique doit donc avoir la même activité thérapeutique que la spécialité princeps, équivalence démontrée par une étude de bioéquivalence. Etant donné que le médicament générique offre la même efficacité et la même qualité, ce dernier est accepté en France. L’efficacité, la qualité et la sécurité des génériques français sont scientifiquement contrôlées et démontrées. Ils ont les mêmes équivalences que le princeps ou médicament de référence. Ils équivalent également aux différentes étapes de vie du princeps."));
        userList.add(new Conseil(
                "LES MÉDICAMENTS GÉNÉRIQUES",
                "Le médicament générique doit donc avoir la même activité thérapeutique que la spécialité princeps, équivalence démontrée par une étude de bioéquivalence. Etant donné que le médicament générique offre la même efficacité et la même qualité, ce dernier est accepté en France. L’efficacité, la qualité et la sécurité des génériques français sont scientifiquement contrôlées et démontrées. Ils ont les mêmes équivalences que le princeps ou médicament de référence. Ils équivalent également aux différentes étapes de vie du princeps."));


        return userList;
    }
*/
}
