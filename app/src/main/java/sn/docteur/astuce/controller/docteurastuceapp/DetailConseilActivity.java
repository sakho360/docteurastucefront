package sn.docteur.astuce.controller.docteurastuceapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class DetailConseilActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_conseil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null){
            getSupportActionBar().setTitle("Détail conseil");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   finish();
            }
        });

        Bundle bundle = getIntent().getExtras();
        String title = bundle.getString("title");
        Log.v("title",title);
        String contenu = bundle.getString("contenu");
        Log.v("contenu",contenu);

        TextView titleTextView = (TextView)findViewById(R.id.title);
        TextView contenuTextView = (TextView)findViewById(R.id.contenu);
        titleTextView.setText(title);
        contenuTextView.setText(contenu);

    }
}
